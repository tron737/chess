<?php

class Desk {
    private $figures = [];
    private $lastColor = null;

    public function __construct() {
        $this->figures['a'][1] = new Rook(false);
        $this->figures['b'][1] = new Knight(false);
        $this->figures['c'][1] = new Bishop(false);
        $this->figures['d'][1] = new Queen(false);
        $this->figures['e'][1] = new King(false);
        $this->figures['f'][1] = new Bishop(false);
        $this->figures['g'][1] = new Knight(false);
        $this->figures['h'][1] = new Rook(false);

        $this->figures['a'][2] = new Pawn(false);
        $this->figures['b'][2] = new Pawn(false);
        $this->figures['c'][2] = new Pawn(false);
        $this->figures['d'][2] = new Pawn(false);
        $this->figures['e'][2] = new Pawn(false);
        $this->figures['f'][2] = new Pawn(false);
        $this->figures['g'][2] = new Pawn(false);
        $this->figures['h'][2] = new Pawn(false);

        $this->figures['a'][7] = new Pawn(true);
        $this->figures['b'][7] = new Pawn(true);
        $this->figures['c'][7] = new Pawn(true);
        $this->figures['d'][7] = new Pawn(true);
        $this->figures['e'][7] = new Pawn(true);
        $this->figures['f'][7] = new Pawn(true);
        $this->figures['g'][7] = new Pawn(true);
        $this->figures['h'][7] = new Pawn(true);

        $this->figures['a'][8] = new Rook(true);
        $this->figures['b'][8] = new Knight(true);
        $this->figures['c'][8] = new Bishop(true);
        $this->figures['d'][8] = new Queen(true);
        $this->figures['e'][8] = new King(true);
        $this->figures['f'][8] = new Bishop(true);
        $this->figures['g'][8] = new Knight(true);
        $this->figures['h'][8] = new Rook(true);
    }

    public function move($move) {
        if (!preg_match('/^([a-h])(\d)-([a-h])(\d)$/', $move, $match)) {
            throw new \Exception("Incorrect move");
        }

        $xFrom = $match[1];
        $yFrom = $match[2];
        $xTo   = $match[3];
        $yTo   = $match[4];

        if (isset($this->figures[$xFrom][$yFrom])) {
            if (!is_null($this->lastColor) && $this->lastColor == $this->figures[$xFrom][$yFrom]->isBlack()) {
                throw new \Exception('Incorrect move');
            }
            if (get_class($this->figures[$xFrom][$yFrom]) == 'Pawn') {
                $distance = ($yTo - $yFrom) < 0 ? ($yTo - $yFrom)*-1 : ($yTo - $yFrom);
                $currentFigure = isset($this->figures[$xTo][$yTo]) ? $this->figures[$xTo][$yTo] : null;
                $currentFromFigure = $this->figures[$xFrom][$yFrom];

                if ((!is_null($currentFigure) && $currentFigure->isBlack() == $currentFromFigure->isBlack()) ||
                    ($xFrom != $xTo && $yFrom != $yTo && $currentFigure->isBlack() == $currentFromFigure->isBlack()) ||
                    (($xFrom == $xTo || $yFrom == $yTo) && ($distance >= 2 && !$currentFromFigure->isFirstMove)) ||
                    (($xFrom == $xTo || $yFrom == $yTo) && !$this->checkAcross($xFrom, $yFrom, $xTo, $yTo))) {
                    throw new \Exception('Invalid move ' . $move);
                }
            }
            $this->figures[$xTo][$yTo] = $this->figures[$xFrom][$yFrom];
            $this->figures[$xTo][$yTo]->isFirstMove = false;
            $this->lastColor = $this->figures[$xTo][$yTo]->isBlack();
        }
        unset($this->figures[$xFrom][$yFrom]);
    }

    public function dump() {
        for ($y = 8; $y >= 1; $y--) {
            echo "$y ";
            for ($x = 'a'; $x <= 'h'; $x++) {
                if (isset($this->figures[$x][$y])) {
                    echo $this->figures[$x][$y];
                } else {
                    echo '-';
                }
            }
            echo "\n";
        }
        echo "  abcdefgh\n";
    }

    private function checkAcross($xFrom, $yFrom, $xTo, $yTo)
    {
        $startXFrom = $xFrom<$xTo ? $xFrom : $xTo;
        $endXFrom = $xFrom>$xTo ? $xFrom : $xTo;

        $startYFrom = $yFrom<$yTo ? $yFrom : $yTo;
        $endYFrom = $yFrom>$yTo ? $yFrom : $yTo;

        for($i = $startXFrom; $i <= $endXFrom; ++$i) {
            for ($j = $startYFrom; $j <= $endYFrom; ++$j) {
                if (isset($this->figures[$i][$j])) {
                    if ($i != $xFrom || $j != $yFrom) {
                        return false;
                    }
                }
            }
        }

        return true;
    }
}
